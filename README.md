# Projet de Programmation Avancée (Systèmes Embarqués 3 - 2021)

Ce dépôt `GIT` contient les livrables de __MM. Clément KOPERSKI et Sami AGGAR__ pour le Projet de Programmation Avancée.
Il est réalisé dans le cadre du Semestre 6 de la filière Systèmes Embarqués de Polytech Lille.
Ce Projet est une application codée en langage C, permettant à un utilisateur de réaliser des requêtes qui interrogent une base de données contenant les
informations de 58492 vols intranationaux, qui ont eu lieu aux Etats-Unis en 2014.



## CONTENU 

Ce dépôt contient à la racine différents répertoires dans lesquels se trouvent les fichiers nécessaires à la réalisation du programme, ces répertoires sont les suivants:
* __`src` :__ Fichiers sources `.c`
     * `hash_functions.c` : Fonctions de hachages + procedure de test (hash_test).
     * `read_data.c` : Lecture des données des fichiers csv + stockage dans les structures de données + procédures d'affichage générique.
     * `requests.c` : Procédures permettant de réaliser l'action de chacune des requêtes formulées par l'utilisateur.
     * `menu.c` : Interface utilisateur + fonctions de manipulation de chaînes de caractères.
     * `main.c` : Ouverture/Fermeture des fichiers csv + appel aux fonctions de lecture + interaction avec les requêtes utilisateur + libération espace mémoire.
* __`includes` :__ Fichiers headers `.h`
     * `data_structures.h` : Inclusion des librairies standard + définition des constantes + définition des structures de données.
     * `hash_functions.h` : Prototypes des procédures de hachage.
     * `read_data.h` : Prototypes des procédures de lecture/affichage des données.
     * `requests.h` : Prototypes des procédures des requêtes.
     * `menu.h` : Prototypes des procédures d'interprétation des requêtes utilisateur.
* __`data` :__
     * `LICENSE` : Certification de droit d'accès libre aux données.
     * `airlines.csv` : Données des 13 compagnies aériennes.
     * `airports.csv` : Données des 322 aéroports.
     * `flights.csv` : Données des 58492 vols.
     * `requetes.txt` : Batterie de requêtes tests.
* D'autres fichiers se trouvent à la racine:
     * __`Makefile` :__ Fichier automatisé établissant les règles de compilation.
     * __`Rapport.pdf` :__ Compte-rendu au format PDF faisant part de nos réflexions sur l'architecture du programme.
     * __`README.md` :__ Fichier que vous êtes actuellement en train de lire !
     * __`.clang-format` :__ Fichier établissant les règles de formatage du langage C (mis à disposition par M. DEQUIDT).
     * __`.gitignore` :__ Ignore l'ajout au dépôt GIT les fichiers spécifiés (pour avoir une copie de travail propre).



## COMPILATION

La compilation de notre programme est pleinement automatisée grâce au Makefile. Le compilateur utilisé est `gcc` avec les options `-g -W -Wall -Wextra -O2`.
Le Makefile utilise des variables internes, des règles de compilation et des commandes bash.

La commande `make` initie les étapes de compilation suivantes:
   * Création des deux répertoires `bin` et `obj` qui contiendront respectivement l'exécutable et les fichiers objets `.o`.
   * Génération des fichiers objets `obj/*.o` à partir des fichiers sources `src/*.c`.
   * Production de l'exécutable `bin/project` à partir des fichiers objets `obj/*.o`.

La commande `make clean` réalise les actions suivantes:
   * Suppression des fichiers objets `obj/*.o`.
   * Suppression du binaire `bin/project`.
   * Suppression des dossiers `bin` et `obj`.

Après avoir récupéré une copie de notre dépôt `GIT`, il faut exécuter la commande `make` à la racine pour générer l'exécutable.
La commande `make clean` permet de supprimer les fichiers compilés.



## UTILISATION DU PROGRAMME

Pour exécuter le programme en interagissant manuellement avec lui, il suffit d'entrer la commande `./bin/project` à la racine.
Pour exécuter le programme avec redirection de l'entrée standard, il suffit d'entrer la commande `./bin/project < data/requetes.txt` à la racine.

Le fonctionnement du programme est cyclique et il exécute les étapes suivantes:
1. Charger le fichier de données
2. Attendre une commande
3. Traiter la commande
4. Afficher le résultat de cette commande
5. Revenir à l'étape 2

Les requêtes renseignables sont les suivantes:

- `show-airports <airline_id>` : Affiche tous les aéroports depuis lesquels la compagnie aérienne <airline_id> opère des vols.
- `show-airlines <airport_id>` : Affiche les compagnies aériennes qui ont des vols au départ de l'aéroport <airport_id>.
- `show-flights <airport_id> <date> [<time>] [limit=<xx>]` : Affiche les vols qui partent de l'aéroport à une certaine date, avec optionnellement une heure de début, et limité à xx vols.
- `most-delayed-flights` : Donne les 5 vols qui ont subi les plus longs retards à l'arrivée.
- `most-delayed-airlines` : Donne les 5 compagnies aériennes qui ont, en moyenne, le plus de temps de retard.
- `delayed-airline <airline_id>` : Donne le temps de retard moyen de la compagnie aérienne passée en paramètre.
- `most-delayed-airlines-at-airport <airport_id>` : Donne les 3 compagnies aériennes avec le plus de retard d'arrivée à l'aéroport passé en paramètre.
- `changed-flights <date>` : Les vols annulés ou déviés à une certaine date.
- `avg-flight-duration <airport_id> <airport_id>` : Calcule le temps de vol moyen entre deux aéroports.
- `find-itinerary <airport_id> <airport_id> <date> [<time>] [limit=<xx>]` : Trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnelle, la requête peut être limitée à xx propositions & il peut y avoir des escales).
- `help` : Affiche la liste des requêtes valides.
- `quit` : Quitte le programme.

Pour information, les paramètres entre crochets `[ ]` sont optionnels et les paramètres entre `< >` indiquent une valeur à renseigner.
Les dates sont au format `MM-DD` et l'heure `HHMM`.

Le programme est permissif en cas de commande invalide. Cependant, dès qu'une commande valide est renseignée, il faut veiller à entrer les arguments dans le même ordre que celui indiqué ci-dessus.
Le programme accepte également n'importe quel fichier de test, comme le `requetes.txt` disponible dans le repertoire `data`. Il est très permissif à la redirection de l'entrée standard !

*Remarque : L'utilisateur peut renseigner jusqu'à 60 retours à la ligne d'affilée (vides de chaîne de caractères) sans que le programme ne se quitte brutalement. Cette spécificité permet d'éviter le bouclage infini du programme en cas d'absence de requête "quit" dans un fichier `.txt` renseigné via redirection de l'entrée standard.*
